function filterBySubstring(array, searched) {
  const wordsContaining = array.filter((phrase) =>
    phrase.toLowerCase().includes(searched.toLowerCase())
  );
  const wordsInitials = array.filter((phrase) => {
    const words = phrase.split(" ");
    if (words.length === searched.length) {
      return words.every((word, index) => {
        return word[0].toLowerCase() === searched[index].toLowerCase();
      });
    }
    return false;
  });
  // get rid of duplicates and sort case-insensitive
  return [...new Set([...wordsContaining, ...wordsInitials])].sort((a, b) =>
    a.toLowerCase().localeCompare(b.toLowerCase())
  );
}
module.exports = filterBySubstring;
