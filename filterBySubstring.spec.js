const filterBySubstring = require("./fileterBySubstring.js");

it("returns the substrings", () => {
  const S = [
    "Wearable",
    "Blind",
    "BLE Transit",
    "Camera",
    "Play Games",
    "Pokemon Go",
  ];
  const K = "bl";

  const result = filterBySubstring(S, K);

  const expectedResult = ["BLE Transit", "Blind", "Wearable"];
  expect(result).toEqual(expectedResult);
});
it("returns the initials", () => {
  const S = [
    "Wearable",
    "Blind",
    "BLE Transit",
    "Camera",
    "Play Games",
    "Pokemon Go",
  ];
  const K = "pg";

  const result = filterBySubstring(S, K);

  const expectedResult = ["Play Games", "Pokemon Go"];
  expect(result).toEqual(expectedResult);
});
it("returns the initials and substrings", () => {
  const S = [
    "Wearable",
    "Blind",
    "BLE Transit",
    "Camera",
    "Play Games",
    "Beach Lazy elegant",
    "Beach Lazy $elegant",
  ];
  const K = "ble";

  const result = filterBySubstring(S, K);

  const expectedResult = ["Beach Lazy elegant", "BLE Transit", "Wearable"];
  expect(result).toEqual(expectedResult);
});
